FROM python:latest
RUN mkdir /app
WORKDIR /app
COPY . .
RUN pip3 install dependency.common.txt
CMD PYTHONPATH=. twistd -n web --port tcp:3000 --wsgi wsgi.app