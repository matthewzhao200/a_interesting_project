provider "aws" {
    region = "us-east-1"
}

resource "aws_vpc" "staging" {
    cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "staging" {
    vpc_id = aws_vpc.staging.id
    cidr_block = "10.0.1.0/24"
    tags = {
        Name = "Staging Subnet"
    }
}

resource "aws_internet_gateway" "gw_staging" {
    vpc_id = aws_vpc.staging.id
}

resource "aws_route" "to_gw_staging" {
    route_table_id = aws_vpc.staging.default_route_table_id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw_staging.id
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_security_group" "allow_staging_communication" {
    name = "allow_staging_communication"
    description = "allow staging k3s to communicate with internet"
    vpc_id = aws_vpc.staging.id
    ingress = {
        description = "K3S API communication"
        from_port = 6443
        to_port = 6443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress = {
        description = "K3S HTTP communication"
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = "Allow staging k3s communcation"
    }
}

resource "aws_key_pair" "deploy_key" {
    key_name = "deploy_key"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDGNVEUBvGGnOg2qY+Mo5fyTloqRjOb9suwbqU6ga4Qyh+aCq1RIbPk/eEVsDFxlc5PM+TDM0016Qp2cTxHci6WMJTN5arQiTIFrdoDAEmPwPrH1/5K8lG+qT94ZQs0O0h7vNdV1rm6oHC8rs68/lNNGQzzzGxsAfszWIpYNcq79c2cCXV7bYEcKDU8vYkTaMDiFU1jILxep0L4i2KlOokqXyQAP5coEdfRe1/ygfTdHRgO3DeHnWfIMI2aguM0rT+i/Qqd+L+jVe8cmuYkLEfjPoq1cOTZUaDT/UlDsE/UxiaxCuGR9mfeAm+P88bzdlzbZ8HEPfNAk3u8llyPijoa0rx4yeTKLIiniPZwN2GLpTCOhV7B/Y/KMXA2vB8SO3NdQYqu9nEHxtLSwFEwp1O/MM+67WqXD5znStbdTWGjeTCCFYcHR9IVvNvhBzzEbfNqFUfc5GzmEGGG8OHIMDEys3iohicOY0DxTyby6O8xawnsnQe3t26KZFRHKQ7HVCU="
}

resource "aws_instance" "k3s" {
    ami = data.aws_ami.ubuntu.id
    instance = "t3a.medium"
    tags = {
        Name = "Staging Server"
    }
    security_groups_ids = [aws_security_group.allow_staging_communication.id]
    subnet_id = aws_subnet.staging.id
    volume_size = 64
    volume_type = "gp2"
    key_name = "deploy_key"
}