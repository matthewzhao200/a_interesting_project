import random
import string
import requests
import flask
app = flask.Flask(__name__)
from . import db
@app.route("/token")
def new_token():
    random_token = ""
    for _ in range(16):
        random_token += random.choice(string.ascii_lowercase)
@app.route("/", methods=["GET", "POST"])
def new_shorten():
    if flask.request.method == "GET":
        return flask.render_template("index.html")
    else:
        url = flask.request.form.get("url")
        token = requests.get("http://localhost/token")
        db.write(token, url)

@app.route("/<token>")
def redirector(token):
    if res := db.read(token) != None:
        return flask.redirect(res), 301

@app.route("/health_check")
def health_check():
    return "success", 200

@app.route("/health_check_db")
def health_check_db():
    if db.health():
        return "success", 200
    else:
        return "failed", 404